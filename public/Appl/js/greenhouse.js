var firebaseConfig = {
    apiKey: "AIzaSyDRz_sPdjJS5x1KrLLeiVY_-YcCRXvyXHU",
    authDomain: "greenhouse-cover.firebaseapp.com",
    databaseURL: "https://greenhouse-cover.firebaseio.com",
    projectId: "greenhouse-cover",
    storageBucket: "greenhouse-cover.appspot.com",
    messagingSenderId: "650619195503",
    appId: "1:650619195503:web:0fc3636d50d9565dc07d9a",
    measurementId: "G-660KTC7CEK"
};
const auth = firebase.auth();
var database = firebase.database();

var refMotoare = database.ref("Motoare");
pageOnLoad();

function pageOnLoad() {
    let perete, slider, actionat, i;
    refMotoare.on('value', function(snapshot) {
        for (i = 1; i <5; i++) {
            perete = "Perete" + i;
            slider = "slider" + i;
            document.getElementById(slider).value = snapshot.val()[perete]["slider"];
        }
        console.log(snapshot.val()["outsideTemp"]);
        document.getElementById("outsideTemp").innerHTML = snapshot.val()["outsideTemp"] + " °C";
        document.getElementById("InsideTemp").innerHTML = snapshot.val()["insideTemp"] + " °C";
        document.getElementById("outsideSoilHumid").innerHTML = snapshot.val()["outsideSoilHumid"] + " %";
        document.getElementById("WindSpeed").innerHTML = snapshot.val()["windSpeed"] + " km/h";
        document.getElementById("insideSoilHumid").innerHTML = snapshot.val()["insideSoilHumid"] + " %";
        
    });

}

function submit(buttonNum) {
    let slider = "slider" + buttonNum;
    let sliderVal = document.getElementById(slider).value;
    let perete = "Perete" + buttonNum;
    firebase.database().ref("Motoare/" + perete).update({
        slider: sliderVal
    });
    pageOnLoad();
}


function setToMin(button) {
    let perete = "Perete" + button;
    firebase.database().ref("Motoare/" + perete).update({
        slider: "4"
    });
}

function setToMax(button) {
    let perete = "Perete" + button;
    let slider = "slider" + button;
    firebase.database().ref("Motoare/" + perete).update({
        slider: "0"
    });
}

function setToMinAll() {
    let perete;
    let slider;
    let index;
    for (index = 1; index < 12; index++) {
        perete = "Perete" + index;
        slider = "slider" + index;
        console.log(perete);
        firebase.database().ref("Motoare/" + perete).update({
            slider: "4"
        });
    }
}

function setToMaxAll() {
    let perete;
    let slider;
    let index;
    for (index = 1; index < 12; index++) {
        perete = "Perete" + index;
        slider = "slider" + index;
        firebase.database().ref("Motoare/" + perete).update({
            slider: "0"
        });
    }
}

function submitAll() {
    let perete;
    let slider;
    let index;
    let value = document.getElementById("slider11").value;
    for (index = 1; index < 12; index++) {
        perete = "Perete" + index;
        slider = "slider" + index;
        firebase.database().ref("Motoare/" + perete).update({
            slider: value
        });
    }
}