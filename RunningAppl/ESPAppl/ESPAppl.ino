#include <WiFi.h>
#include <FirebaseESP32.h>
#include "time.h"

const char* ssid = "DigiMacarie-TV";
const char* password =  "baritiu37";

FirebaseData firebaseData;
FirebaseJson json;


const char* ntpServer = "pool.ntp.org";
const long  gmtOffset_sec = 7200;
const int   daylightOffset_sec = 3600;
struct tm timeinfo;
void printResult(FirebaseData &data);
String value;
int rez;
void setup() {
  Serial.begin(115200);
  WiFiConnection();
  connectToSrv();
  init();
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
}

void init()
{
  initBoard();
}

void initBoard()
{
  pinMode(Perete1Up, OUTPUT);
  pinMode(Perete2Up, OUTPUT);
  pinMode(Perete3Up, OUTPUT);
  pinMode(Perete4Up, OUTPUT);
  pinMode(Perete5Up, OUTPUT);
  pinMode(Perete6Up, OUTPUT);
  pinMode(Perete7Up, OUTPUT);
  pinMode(Perete8Up, OUTPUT);
  pinMode(Perete9Up, OUTPUT);
  pinMode(Perete10Up, OUTPUT);
  pinMode(Perete1Dn, OUTPUT);
  pinMode(Perete2Dn, OUTPUT);
  pinMode(Perete3Dn, OUTPUT);
  pinMode(Perete4Dn, OUTPUT);
  pinMode(Perete5Dn, OUTPUT);
  pinMode(Perete6Dn, OUTPUT);
  pinMode(Perete7Dn, OUTPUT);
  pinMode(Perete8Dn, OUTPUT);
  pinMode(Perete9Dn, OUTPUT);
  pinMode(Perete10Dn, OUTPUT);
  digitalWrite(Perete1Up, LOW);
  digitalWrite(Perete2Up, LOW);
  digitalWrite(Perete3Up, LOW);
  digitalWrite(Perete4Up, LOW);
  digitalWrite(Perete5Up, LOW);
  digitalWrite(Perete6Up, LOW);
  digitalWrite(Perete7Up, LOW);
  digitalWrite(Perete8Up, LOW);
  digitalWrite(Perete9Up, LOW);
  digitalWrite(Perete1Dn, LOW);
  digitalWrite(Perete2Dn, LOW);
  digitalWrite(Perete3Dn, LOW);
  digitalWrite(Perete4Dn, LOW);
  digitalWrite(Perete5Dn, LOW);
  digitalWrite(Perete6Dn, LOW);
  digitalWrite(Perete7Dn, LOW);
  digitalWrite(Perete8Dn, LOW);
  digitalWrite(Perete9Dn, LOW);
  digitalWrite(Perete10Dn, LOW);
}

void WiFiConnection()
{
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Connecting to WiFi..");
  }
  Serial.println("Connected to the WiFi network");
}

void connectToSrv()
{
  Firebase.connection("USsrvMain.io","sfYb45B96H62bGi9DDT");
  Firebase.reconnectWiFi(true);
  String perete = "Motoare/Services/Local";
  Firebase.set(firebaseData, perete, ssid);
}

void loop() {
  Firebase.getString(firebaseData, "Motoare/Services");
  FirebaseJson &json = firebaseData.jsonObject();
  json.iteratorBegin();
  String key, value, value2 = "";
  getLocalTime(&timeinfo);
  char timeHour[3], timeMin[3], timeSec[3], dateDay[10], dateMonth[10];
  strftime(timeHour, 10, "%H", &timeinfo);
  strftime(timeMin, 10, "%M", &timeinfo);
  strftime(timeSec, 10, "%S", &timeinfo);
  strftime(dateDay, 10, "%d", &timeinfo);
  strftime(dateMonth, 10, "%B", &timeinfo);
  String lastOnline = String(dateDay) + " " + String(dateMonth) + "  |  " + String(timeHour) + ":" + String(timeMin) + ":" + String(timeSec);
  Firebase.set(firebaseData, "Motoare/Services/LastOnline", String(lastOnline));
  int type = 0;
  json.iteratorGet(0, type, key, value);
  if (value == "1")
  {
    for (int index = 1; index < 11; index++)
    {
      String path =  "Motoare/Perete" + String(index);
      Firebase.getString(firebaseData, path);
      FirebaseJson &json = firebaseData.jsonObject();
      json.iteratorBegin();
      json.iteratorGet(0, type, key, value);    //act
      json.iteratorGet(3, type, key, value2);   //des
      if (!value.equals(value2))
      {
        rez = value.toInt() - value2.toInt();
        unsigned long milli = abs(rez) * 10000;
        String pin = "Perete" + String(index) + "Up";
        String perete = "Motoare/Perete" + String(index) + "/active";
        Firebase.set(firebaseData, perete, "true");
        if (rez < 0)  //UP
        {
          moveUpWall(index - 1, abs(rez));
        }
        else          //DN
        {
          moveDnWall(index - 1, abs(rez));
        }
        Firebase.set(firebaseData, perete, "false");
        Firebase.set(firebaseData, "Motoare/Perete" + String(index) + "/actSlider", value2);
        getLocalTime(&timeinfo);
        strftime(timeHour, 10, "%H", &timeinfo);
        strftime(timeMin, 10, "%M", &timeinfo);
        strftime(timeSec, 10, "%S", &timeinfo);
        strftime(dateDay, 10, "%d", &timeinfo);
        strftime(dateMonth, 10, "%B", &timeinfo);
        String totalTime = String(dateDay) + " " + String(dateMonth) + "  |  " + String(timeHour) + ":" + String(timeMin) + ":" + String(timeSec);
        Firebase.set(firebaseData, "Motoare/Perete" + String(index) + "/lastTime", String(totalTime));
      }
    }
  }
}

void moveUpWall(int index, int rez)
{
  switch (index)
  {
    case 0:
      digitalWrite(Perete1Up, HIGH);
      delay(rez * 10000);
      digitalWrite(Perete1Up, LOW);
      break;
    case 1:
      digitalWrite(Perete2Up, HIGH);
      delay(rez * 10000);
      digitalWrite(Perete2Up, LOW);
      break;
    case 2:
      digitalWrite(Perete3Up, HIGH);
      delay(rez * 10000);
      digitalWrite(Perete3Up, LOW);
      break;
    case 3:
      digitalWrite(Perete4Up, HIGH);
      delay(rez * 10000);
      digitalWrite(Perete4Up, LOW);
      break;
    case 4:
      digitalWrite(Perete5Up, HIGH);
      delay(rez * 10000);
      digitalWrite(Perete5Up, LOW);
      break;
    case 5:
      digitalWrite(Perete6Up, HIGH);
      delay(rez * 10000);
      digitalWrite(Perete6Up, LOW);
      break;
    case 6:
      digitalWrite(Perete7Up, HIGH);
      delay(rez * 10000);
      digitalWrite(Perete7Up, LOW);
      break;
    case 7:
      digitalWrite(Perete8Up, HIGH);
      delay(rez * 10000);
      digitalWrite(Perete8Up, LOW);
      break;
    case 8:
      digitalWrite(Perete9Up, HIGH);
      delay(rez * 10000);
      digitalWrite(Perete9Up, LOW);
      break;

    case 9:
      digitalWrite(Perete10Up, HIGH);
      delay(rez * 10000);
      digitalWrite(Perete10Up, LOW);
      break;
  }
}

void moveDnWall(int index, int rez)
{
  switch (index)
  {
    case 0:
      digitalWrite(Perete1Dn, HIGH);
      delay(rez * 10000);
      digitalWrite(Perete1Dn, LOW);
      break;
    case 1:
      digitalWrite(Perete2Dn, HIGH);
      delay(rez * 10000);
      digitalWrite(Perete2Dn, LOW);
      break;
    case 2:
      digitalWrite(Perete3Dn, HIGH);
      delay(rez * 10000);
      digitalWrite(Perete3Dn, LOW);
      break;
    case 3:
      digitalWrite(Perete4Dn, HIGH);
      delay(rez * 10000);
      digitalWrite(Perete4Dn, LOW);
      break;
    case 4:
      digitalWrite(Perete5Dn, HIGH);
      delay(rez * 10000);
      digitalWrite(Perete5Dn, LOW);
      break;
    case 5:
      digitalWrite(Perete6Dn, HIGH);
      delay(rez * 10000);
      digitalWrite(Perete6Dn, LOW);
      break;
    case 6:
      digitalWrite(Perete7Dn, HIGH);
      delay(rez * 10000);
      digitalWrite(Perete7Dn, LOW);
      break;
    case 7:
      digitalWrite(Perete8Dn, HIGH);
      delay(rez * 10000);
      digitalWrite(Perete8Dn, LOW);
      break;
    case 8:
      digitalWrite(Perete9Dn, HIGH);
      delay(rez * 10000);
      digitalWrite(Perete9Dn, LOW);
      break;
    case 9:
      digitalWrite(Perete10Dn, HIGH);
      delay(rez * 10000);
      digitalWrite(Perete10Dn, LOW);
      break;
  }
}
